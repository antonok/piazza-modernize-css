# piazza-modernize-css

This repository contains a simple CSS user style that makes Piazza (in my opinion) a lot easier to look at. Piazza's built-in theme makes heavy use of gradients and shadows for a "realistic" look, which would have been nice in the mid '00s. More recent web design trends use bold, flat colors to achieve a clean look.

## how to use

If you've installed a browser extension for user styles (I prefer [Stylus](https://github.com/openstyles/stylus)), you can simply create a new user style and paste the contents of the userStyle.css file in this repo. Set the domain to be `https://piazza.com`, and you should be able to see the new theme.
